package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Fabriquant;
import metier.Marque;
import metier.Pays;


public class MarqueDAO extends DAO {

	public MarqueDAO(Connection connexion) {
		super(connexion);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getByID(int id) {

		ResultSet rs;
		Marque marque = null;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM MARQUE LEFT JOIN PAYS ON PAYS.ID_PAYS=MARQUE.ID_PAYS LEFT JOIN FABRICANT ON FABRICANT.ID_FABRICANT = MARQUE.ID_FABRICANT WHERE ID_MARQUE = " + id ;
			rs = stmt.executeQuery(cmd);
			marque = new Marque(rs.getInt(1), rs.getString(2),new Pays(rs.getInt(3),rs.getString(6)), new Fabriquant(rs.getInt(4),rs.getString(9)) );
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (T) marque;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ArrayList<T> getAll() {
		ResultSet rs;
		ArrayList<Marque> listeMarque = new ArrayList();

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM MARQUE LEFT JOIN PAYS ON PAYS.ID_PAYS=MARQUE.ID_PAYS LEFT JOIN FABRICANT ON FABRICANT.ID_FABRICANT = MARQUE.ID_FABRICANT ORDER BY MARQUE.NOM_MARQUE";
			rs = stmt.executeQuery(cmd);
			while (rs.next()) {
				listeMarque.add(new Marque(rs.getInt(1), rs.getString(2),new Pays(rs.getInt(3),rs.getString(6)), new Fabriquant(rs.getInt(4),rs.getString(9))));

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ArrayList<T>) listeMarque;
	}

	@Override
	public boolean insert(Object o) {
		boolean isOk = false;


		return isOk;
	}

	@Override
	public boolean update(Object o) {
		boolean isOk = false;

		return isOk;
	}

	@Override
	public boolean delete(Object o) {
		boolean isOk = false;

		return isOk;
	}

}
