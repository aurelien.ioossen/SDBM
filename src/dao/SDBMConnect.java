package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class SDBMConnect {


	
	private static Connection connexion;

	public SDBMConnect() {
		
		String dbUrl="jdbc:sqlserver://STA7400562:1433;databaseName=SDBM";
		String user="java_user" ;
		String pass="1234" ;

		try {
			connexion = DriverManager.getConnection(dbUrl,user,pass);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static Connection getInstance() {
		
		if (connexion == null) {
			
			new SDBMConnect();
		}
		return connexion;
		
	}
	
	
}
