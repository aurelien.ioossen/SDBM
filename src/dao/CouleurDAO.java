package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Couleur;

public class CouleurDAO extends DAO {

	public CouleurDAO(Connection connexion) {
		super(connexion);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getByID(int id) {

		ResultSet rs;
		Couleur color = null;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM COULEUR WHERE ID_COULEUR = " + id;
			rs = stmt.executeQuery(cmd);
			color = new Couleur(rs.getInt(1), rs.getString(2));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (T) color;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ArrayList<T> getAll() {
		ResultSet rs;
		ArrayList<Couleur> listeCouleurs = new ArrayList();

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM COULEUR ORDER BY COULEUR.NOM_COULEUR";
			rs = stmt.executeQuery(cmd);
			while (rs.next()) {
				listeCouleurs.add(new Couleur(rs.getInt(1), rs.getString(2)));
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ArrayList<T>) listeCouleurs;
	}

	@Override
	public boolean insert(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "INSERT INTO COULEUR (ID_COULEUR,NOM_COULEUR) VALUES (" + ((Couleur) o).getId_couleur() + ","
					+ ((Couleur) o).getCouleur() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean update(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "UPDATE FROM COULEUR SET  VALUES (" + ((Couleur) o).getId_couleur() + ","
					+ ((Couleur) o).getCouleur() + ") WHERE ID_COULEUR = " + ((Couleur) o).getId_couleur();
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean delete(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "DELETE FROM COULEUR WHERE ID_COULEUR = (" + ((Couleur) o).getId_couleur() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

}
