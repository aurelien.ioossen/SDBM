package dao;

import java.sql.Connection;

import metier.Fabriquant;

public class DAOFactory {

	private static final Connection connexion = SDBMConnect.getInstance();
	
	public static CouleurDAO getInstanceCouleurDAO() {return new CouleurDAO(connexion);	}

	public static TypeDAO getInstanceTypeDAO() {
		return new TypeDAO(connexion);
	}

	public static PaysDAO getInstancePaysDAO() {
		return new PaysDAO(connexion);
	}
	
	public static ContinentDAO getInstanceContinentDAO() {
		return new ContinentDAO(connexion);
	}

	public static MarqueDAO getInstanceMarqueDAO() {
		return new MarqueDAO(connexion) ;
	}

	public static FabriquantDAO getInstanceFabriquantDAO() {
		return new FabriquantDAO(connexion);
	}
	
	
	public static ArticleDAO getInstanceArticleDAO() {
		return new ArticleDAO(connexion);
	}
	
}
