package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Couleur;
import metier.Fabriquant;
import metier.Marque;
import metier.Pays;
import metier.Type;

public class FabriquantDAO extends DAO {

	Fabriquant newCreated ;
	
	public FabriquantDAO(Connection connexion) {
		super(connexion);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getByID(int id) {

		ResultSet rs;
		Fabriquant fab = null;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM FABRICANT WHERE ID_FABRICANT = " + id;
			rs = stmt.executeQuery(cmd);
			fab = new Fabriquant(rs.getInt(1), rs.getString(2));
			rs.close();
		} 
		
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (T) fab;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ArrayList<T> getAll() {
		ResultSet rs;
		ArrayList<Fabriquant> listeFabricant = new ArrayList();
		newCreated = new Fabriquant(0, "") ;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM FABRICANT LEFT JOIN MARQUE ON MARQUE.ID_FABRICANT = FABRICANT.ID_FABRICANT ORDER BY NOM_FABRICANT";
			rs = stmt.executeQuery(cmd);
			while (rs.next()) {

				if(newCreated.getId_fabriquant() != rs.getInt(1)) {
					
					newCreated = new Fabriquant(rs.getInt(1),rs.getString(2));
					listeFabricant.add(newCreated);
					
				}
				newCreated.AddMarque(new Marque(rs.getInt(3), rs.getString(4)));	
							
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ArrayList<T>) listeFabricant;
	}

	@Override
	public boolean insert(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "INSERT INTO FABRICANT (ID_FABRICANT,NOM_FABRICANT) VALUES (" + ((Fabriquant) o).getId_fabriquant() + ","
					+ ((Type) o).getType() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean update(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "UPDATE FROM FABRICANT SET  VALUES (" + ((Fabriquant) o).getId_fabriquant() + ","
					+ ((Fabriquant) o).getFabriquant() + ") WHERE ID_FABRICANT = " + ((Fabriquant) o).getId_fabriquant();
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean delete(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "DELETE FROM FABRICANT WHERE ID_FABRICANT = (" + ((Fabriquant) o).getId_fabriquant() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

}
