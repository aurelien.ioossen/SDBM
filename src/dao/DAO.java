package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class DAO {

	protected Connection connexion ;

	public DAO(Connection connexion) {

		this.connexion = connexion;
	}
	
	public abstract <T> T getByID(int id);
	public abstract <T> ArrayList<T> getAll();
	public abstract boolean insert(Object o);
	public abstract boolean update(Object o);
	public abstract boolean delete(Object o) throws SQLException;
	
	
	
	
	
	
}
