package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.Article;
import metier.Continent;
import metier.Couleur;
import metier.Fabriquant;
import metier.Marque;
import metier.Pays;
import metier.Type;
import service.ArticleSearched;

public class ArticleDAO extends DAO {

	public ArticleDAO(Connection connexion) {
		super(connexion);
	}

	@Override
	public <T> ArrayList<T> getAll() {
		ResultSet rs = null;
		ArrayList<Article> listeArticles = new ArrayList<>();
		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM VUE_ARTICLE ORDER BY NOM_ARTICLE  ";
			rs = stmt.executeQuery(cmd);

			while (rs.next()) {

				Article art = new Article(rs.getInt(1), rs.getString(2), rs.getBigDecimal(3), rs.getInt(4),
						rs.getFloat(5));
				art.setCouleur(new Couleur(rs.getInt(15), rs.getString(10)));
				art.setMarque(new Marque(rs.getInt(17), rs.getString(12),
						new Pays(rs.getInt(6), rs.getString(7), new Continent(rs.getInt(8), rs.getString(9))),
						new Fabriquant(rs.getInt(14), rs.getString(13))

				));
				art.setType(new Type(rs.getInt(16), rs.getString(11)));

				listeArticles.add(art);

			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ArrayList<T>) listeArticles;

	}

	@Override
	public <T> T getByID(int id) {
		ResultSet rs;
		Article art = null;

		return (T) art;

	}

	@Override
	public boolean insert(Object o) {
		Article art = (Article) o;

		try {
			String sql = "INSERT INTO ARTICLE(NOM_ARTICLE,PRIX_ACHAT,VOLUME,TITRAGE,ID_MARQUE,ID_COULEUR,ID_TYPE) VALUES (?,?,?,?,?,?,?)";
			PreparedStatement ps = connexion.prepareStatement(sql);
			ps.setString(1, art.getNom_article());
			ps.setBigDecimal(2, art.getPrix_achat());
			if (art.getVolume() == 0) {
				ps.setNull(3, java.sql.Types.INTEGER);

			} else {

				ps.setInt(3, ((Article) o).getVolume());
			}
			if (art.getTitrage() == 0) {
				ps.setNull(4, java.sql.Types.FLOAT);
				
			} else {
				ps.setFloat(4, ((Article) o).getTitrage());

			}
			ps.setInt(5, ((Article) o).getMarque().getId_marque());

			if (art.getCouleur().getId_couleur() == 0) {

				ps.setNull(6, java.sql.Types.INTEGER);

			} else {
				ps.setInt(6, ((Article) o).getCouleur().getId_couleur());

			}

			if (art.getType().getId_type() == 0) {

				ps.setNull(7, java.sql.Types.INTEGER);

			} else {
				ps.setInt(7, art.getType().getId_type());
			}

			System.out.println(sql);
			System.out.println("VOLUME :" + art.getVolume());

			ps.execute();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	public boolean update(Object o) {
		Article art = (Article) o;
		ResultSet rs = null;
        boolean isOk =false;
		Statement stmt;
		try {

			String sql = "UPDATE ARTICLE  SET  NOM_ARTICLE = ? ,PRIX_ACHAT = ? ,VOLUME= ? ,TITRAGE= ?,ID_MARQUE= ?,ID_COULEUR = ?,ID_TYPE= ? WHERE ID_ARTICLE = ?";
			PreparedStatement ps = connexion.prepareStatement(sql);

			ps.setString(1, ((Article) o).getNom_article());
			ps.setBigDecimal(2, ((Article) o).getPrix_achat());
			ps.setInt(3, ((Article) o).getVolume());
			ps.setFloat(4, ((Article) o).getTitrage());
			ps.setInt(5, ((Article) o).getMarque().getId_marque());

			if (art.getCouleur().getId_couleur() == 0) {

				ps.setNull(6, java.sql.Types.INTEGER);

			} else {
				ps.setInt(6, ((Article) o).getCouleur().getId_couleur());

			}

			if (art.getType().getId_type() == 0) {

				ps.setNull(7, java.sql.Types.INTEGER);

			} else {
				ps.setInt(7, art.getType().getId_type());
			}

			ps.setInt(8, ((Article) o).getId_article());

			 isOk = ps.execute();
			ps.close();

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return isOk;

	}

	@Override
	public boolean delete(Object o) throws SQLException {
		PreparedStatement ps;
		String sql = "DELETE FROM ARTICLE  WHERE ID_ARTICLE = ?";
		ps = connexion.prepareStatement(sql);
		ps.setInt(1, ((Article) o).getId_article());
		ps.execute();

		return false;
	}

	@SuppressWarnings({ "unchecked" })
	public ObservableList<Article> filtrerArticle(ArticleSearched artSearched) {

		ArrayList<Article> listeArticles = new ArrayList<>();
		ResultSet rs = null;
		String procedure_stockee = "{call dbo.sp_articlesQBE(?,?,?,?,?,?,?,?,?,?)}";

		try {

			CallableStatement sctmt = this.connexion.prepareCall(procedure_stockee);
			if (artSearched.getNomSearched() == null) {
				sctmt.setNull(1, java.sql.Types.NVARCHAR);
			} else {
				sctmt.setString(1, artSearched.getNomSearched());

			}

			if (artSearched.getVolume() == null) {
				sctmt.setNull(2, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(2, artSearched.getVolume());
			}

			if (artSearched.getMarque() == null || artSearched.getMarque().getId_marque() == 0) {
				sctmt.setNull(3, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(3, artSearched.getMarque().getId_marque());
			}

			if (artSearched.getTitrageMin() == null) {
				sctmt.setNull(4, java.sql.Types.FLOAT);
			} else {
				sctmt.setDouble(4, artSearched.getTitrageMin());

			}

			if (artSearched.getTitrageMax() == null) {
				sctmt.setNull(5, java.sql.Types.FLOAT);
			} else {
				sctmt.setDouble(5, artSearched.getTitrageMax());

			}

			if (artSearched.getCouleur() == null || artSearched.getCouleur().getId_couleur() == 0) {
				sctmt.setNull(6, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(6, artSearched.getCouleur().getId_couleur());

			}

			if (artSearched.getType() == null || artSearched.getType().getId_type() == 0) {
				sctmt.setNull(7, java.sql.Types.INTEGER);
			} else {
				sctmt.setDouble(7, artSearched.getType().getId_type());

			}

			if (artSearched.getPays() == null || artSearched.getPays().getId_pays() == 0) {
				sctmt.setNull(8, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(8, artSearched.getPays().getId_pays());

			}

			if (artSearched.getContinent() == null || artSearched.getContinent().getId_continent() == 0) {
				sctmt.setNull(9, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(9, artSearched.getContinent().getId_continent());

			}

			if (artSearched.getFabriquant() == null || artSearched.getFabriquant().getId_fabriquant() == 0) {
				sctmt.setNull(10, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(10, artSearched.getFabriquant().getId_fabriquant());

			}

			sctmt.execute();
			rs = sctmt.getResultSet();

			while (rs.next()) {

				Article art = new Article(rs.getInt(1), rs.getString(2), rs.getBigDecimal(3), rs.getInt(4),
						rs.getFloat(5));
				art.setCouleur(new Couleur(rs.getInt(15), rs.getString(10)));
				art.setMarque(new Marque(rs.getInt(17), rs.getString(12),
						new Pays(rs.getInt(6), rs.getString(7), new Continent(rs.getInt(8), rs.getString(9))),
						new Fabriquant(rs.getInt(14), rs.getString(13))

				));
				art.setType(new Type(rs.getInt(16), rs.getString(11)));

				listeArticles.add(art);

			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return FXCollections.observableArrayList(listeArticles);
	}

	public double getTitrageMin(ArticleSearched artSearched) {
		Double titrage_min = null;
		ResultSet rs = null;
		String procedure_stockee = "{call dbo.sp_min_max_titrage(?,?,?,?,?,?,?,?)}";



		try {

			CallableStatement sctmt = this.connexion.prepareCall(procedure_stockee);
			if (artSearched.getNomSearched() == null) {
				sctmt.setNull(1, java.sql.Types.NVARCHAR);
			} else {
				sctmt.setString(1, artSearched.getNomSearched());
			}

			if (artSearched.getVolume() == null) {
				sctmt.setNull(2, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(2, artSearched.getVolume());
			}

			if (artSearched.getMarque() == null || artSearched.getMarque().getId_marque() == 0) {
				sctmt.setNull(3, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(3, artSearched.getMarque().getId_marque());
			}

			if (artSearched.getCouleur() == null || artSearched.getCouleur().getId_couleur() == 0) {
				sctmt.setNull(4, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(4, artSearched.getCouleur().getId_couleur());

			}

			if (artSearched.getType() == null || artSearched.getType().getId_type() == 0) {
				sctmt.setNull(5, java.sql.Types.INTEGER);
			} else {
				sctmt.setDouble(5, artSearched.getType().getId_type());

			}

			if (artSearched.getPays() == null || artSearched.getPays().getId_pays() == 0) {
				sctmt.setNull(6, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(6, artSearched.getPays().getId_pays());

			}

			if (artSearched.getContinent() == null || artSearched.getContinent().getId_continent() == 0) {
				sctmt.setNull(7, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(7, artSearched.getContinent().getId_continent());

			}

			if (artSearched.getFabriquant() == null || artSearched.getFabriquant().getId_fabriquant() == 0) {
				sctmt.setNull(8, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(8, artSearched.getFabriquant().getId_fabriquant());

			}

			sctmt.execute();
			rs = sctmt.getResultSet();

			while (rs.next()) {
				titrage_min = rs.getDouble(1);
			}
			System.out.println(rs);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return titrage_min;
	}

	public double getTitrageMax(ArticleSearched artSearched) {
		Double titrage_max = null;
		ResultSet rs = null;
		String procedure_stockee = "{call dbo.sp_min_max_titrage(?,?,?,?,?,?,?,?)}";
		try {

			CallableStatement sctmt = this.connexion.prepareCall(procedure_stockee);
			if (artSearched.getNomSearched() == null) {
				sctmt.setNull(1, java.sql.Types.NVARCHAR);
			} else {
				sctmt.setString(1, artSearched.getNomSearched());
			}

			if (artSearched.getVolume() == null) {
				sctmt.setNull(2, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(2, artSearched.getVolume());
			}

			if (artSearched.getMarque() == null || artSearched.getMarque().getId_marque() == 0) {
				sctmt.setNull(3, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(3, artSearched.getMarque().getId_marque());
			}

			if (artSearched.getCouleur() == null || artSearched.getCouleur().getId_couleur() == 0) {
				sctmt.setNull(4, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(4, artSearched.getCouleur().getId_couleur());

			}

			if (artSearched.getType() == null || artSearched.getType().getId_type() == 0) {
				sctmt.setNull(5, java.sql.Types.INTEGER);
			} else {
				sctmt.setDouble(5, artSearched.getType().getId_type());

			}

			if (artSearched.getPays() == null || artSearched.getPays().getId_pays() == 0) {
				sctmt.setNull(6, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(6, artSearched.getPays().getId_pays());

			}

			if (artSearched.getContinent() == null || artSearched.getContinent().getId_continent() == 0) {
				sctmt.setNull(7, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(7, artSearched.getContinent().getId_continent());

			}

			if (artSearched.getFabriquant() == null || artSearched.getFabriquant().getId_fabriquant() == 0) {
				sctmt.setNull(8, java.sql.Types.INTEGER);
			} else {
				sctmt.setInt(8, artSearched.getFabriquant().getId_fabriquant());

			}

			sctmt.execute();
			rs = sctmt.getResultSet();

			while (rs.next()) {
				titrage_max = rs.getDouble(2);
			}
			System.out.println(rs);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return titrage_max;
	}

}
