package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Couleur;
import metier.Type;

public class TypeDAO extends DAO {

	public TypeDAO(Connection connexion) {
		super(connexion);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getByID(int id) {

		ResultSet rs;
		Type type = null;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM TYPEBIERE WHERE ID_TYPE = " + id;
			rs = stmt.executeQuery(cmd);
			type = new Type(rs.getInt(1), rs.getString(2));
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (T) type;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ArrayList<T> getAll() {
		ResultSet rs;
		ArrayList<Type> listeType = new ArrayList();

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM TYPEBIERE ORDER BY NOM_TYPE";
			rs = stmt.executeQuery(cmd);
			while (rs.next()) {
				listeType.add(new Type(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ArrayList<T>) listeType;
	}

	@Override
	public boolean insert(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "INSERT INTO TYPEBIERE (ID_TYPE,NOM_TYPE) VALUES (" + ((Type) o).getId_type() + ","
					+ ((Type) o).getType() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean update(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "UPDATE FROM TYPEBIERE SET  VALUES (" + ((Type) o).getId_type() + ","
					+ ((Type) o).getType() + ") WHERE ID_TYPE = " + ((Type) o).getId_type();
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean delete(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "DELETE FROM TYPEBIERE WHERE ID_TYPE = (" + ((Type) o).getId_type() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

}
