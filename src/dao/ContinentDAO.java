package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Continent;
import metier.Pays;

public class ContinentDAO extends DAO {

	Continent newCreated;

	public ContinentDAO(Connection connexion) {
		super(connexion);

		newCreated = null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getByID(int id) {

		ResultSet rs;
		Continent continent = null;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM CONTINENT JOIN PAYS ON PAYS.ID_CONTINENT = CONTINENT.ID_CONTINENT WHERE ID_CONTINENT = "
					+ id;
			rs = stmt.executeQuery(cmd);
			continent = new Continent(rs.getInt(1), rs.getString(2));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (T) continent;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ArrayList<T> getAll() {
		ResultSet rs;
		ArrayList<Continent> listeContinent = new ArrayList();
		newCreated = new Continent(0, "");

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM CONTINENT JOIN PAYS ON PAYS.ID_CONTINENT = CONTINENT.ID_CONTINENT ORDER BY CONTINENT.NOM_CONTINENT";
			rs = stmt.executeQuery(cmd);

			while (rs.next()) {

				if (newCreated.getId_continent() != rs.getInt(1)) {
					
					newCreated = new Continent(rs.getInt(1), rs.getString(2)) ;
					listeContinent.add(newCreated);
					
				}
				newCreated.AddPays(new Pays(rs.getInt(3), rs.getString(4)));

			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ArrayList<T>) listeContinent;
	}

	@Override
	public boolean insert(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "INSERT INTO CONTINENT (ID_CONTINENT,NOM_CONTINENT) VALUES ("
					+ ((Continent) o).getId_continent() + "," + ((Continent) o).getContinent() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean update(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "UPDATE FROM CONTINENT SET  VALUES (" + ((Continent) o).getId_continent() + ","
					+ ((Continent) o).getContinent() + ") WHERE ID_Continent = " + ((Continent) o).getId_continent();
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean delete(Object o) {
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "DELETE FROM CONTINENT WHERE ID_CONTINENT = (" + ((Continent) o).getId_continent() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOk;
	}

}
