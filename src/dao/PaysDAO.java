package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Continent;
import metier.Couleur;
import metier.Pays;
import metier.Type;

public class PaysDAO extends DAO {

	public PaysDAO(Connection connexion) {
		super(connexion);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getByID(int id) {

		ResultSet rs;
		Pays pays = null;

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM PAYS JOIN CONTINENT ON PAYS.ID_CONTINENT=CONTINENT.ID_CONTINENT WHERE ID_PAYS = " + id;
			rs = stmt.executeQuery(cmd);
			pays = new Pays(rs.getInt(1), rs.getString(2),new Continent(rs.getInt(3),rs.getString(5)));
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (T) pays;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ArrayList<T> getAll() {
		ResultSet rs;
		ArrayList<Pays> listePays = new ArrayList();

		try {
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM PAYS JOIN CONTINENT ON PAYS.ID_CONTINENT=CONTINENT.ID_CONTINENT ORDER BY PAYS.NOM_PAYS";
			rs = stmt.executeQuery(cmd);
			while (rs.next()) {
				listePays.add(new Pays(rs.getInt(1), rs.getString(2),new Continent(rs.getInt(3),rs.getString(5))));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ArrayList<T>) listePays;
	}

	@Override
	public boolean insert(Object o) {
		return false;
		//TODO
	}

	@Override
	public boolean update(Object o) {
		return false;
		//TODO
	}

	@Override
	public boolean delete(Object o) {
		return false;
		//TODO

	}

}
