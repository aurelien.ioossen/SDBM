package view;


import application.SDBM;
import com.jfoenix.controls.JFXSlider;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.print.PrinterJob;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.AnchorPane;
import metier.*;
import service.ArticleBean;

import java.awt.print.PrinterException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Optional;

public class SdbmControler {

    AnchorPane anchorPane;
    ArticleBean art;
    @FXML
    TableView<Article> tableArticle;
    private SDBM sdbm;
    @FXML
    private ComboBox<Couleur> cb_couleur;
    @FXML
    private ComboBox<Type> cb_type;
    @FXML
    private ComboBox<Pays> cb_pays;
    @FXML
    private ComboBox<Continent> cb_continent;
    @FXML
    private ComboBox<Marque> cb_marque;
    @FXML
    private ComboBox<Fabriquant> cb_fabriquant;
    @FXML
    private TableColumn<Article, String> colonneNom;
    @FXML
    private TableColumn<Article, BigDecimal> colonnePrix;
    @FXML
    private TableColumn<Article, Integer> colonneVolume;
    @FXML
    private TableColumn<Article, Marque> colonneMarque;
    @FXML
    private TableColumn<Article, Fabriquant> colonneFabriquant;
    @FXML
    private TableColumn<Article, Type> colonneType;
    @FXML
    private TableColumn<Article, Couleur> colonneCouleur;
    @FXML
    private TableColumn<Article, Continent> colonneContinent;
    @FXML
    private TableColumn<Article, Pays> colonnePays;
    @FXML
    private TableColumn<Article, Float> colonneTitrage;
    @FXML
    private TextField tf_recherche;
    @FXML
    private ToggleGroup tg_volume;
    @FXML
    private RadioButton rb_33;
    @FXML
    private RadioButton rb_75;
    @FXML
    private JFXSlider sl_titrage_min;
    @FXML
    private JFXSlider sl_titrage_max;
    @FXML
    private TextField tf_count_article;
    @FXML
    private Button btn_modifier;
    @FXML
    private Button btn_supprimer;


    @FXML
    private void initialize() { }

    private void setSelectedArticle(Article selectedArticle) {
        art.setSelectedArticle(selectedArticle);
        btn_supprimer.setDisable(false);
        btn_modifier.setDisable(false);
        System.out.println("Article SELECTIONNE :" + art.getSelectedArticle());
    }

    @FXML
    private void filtrerArticle() {

        Integer valueButton;
        RadioButton selectedButtonVolume = (RadioButton) tg_volume.getSelectedToggle();
        if (selectedButtonVolume != null && selectedButtonVolume.getId().equals("rb_33")) {
            valueButton = 33;

        } else if (selectedButtonVolume != null && selectedButtonVolume.getId().equals("rb_75")) {
            valueButton = 75;

        } else {
            valueButton = null;
        }
        Integer volume = valueButton;
        String nom_searched = tf_recherche.getText();
        Double titrage_min = sl_titrage_min.getValue();
        Double titrage_max = sl_titrage_max.getValue();
        Continent cnt = cb_continent.getSelectionModel().getSelectedItem();
        Pays pays = cb_pays.getSelectionModel().getSelectedItem();
        Fabriquant fq = cb_fabriquant.getSelectionModel().getSelectedItem();
        Marque mar = cb_marque.getSelectionModel().getSelectedItem();
        Type type = cb_type.getSelectionModel().getSelectedItem();
        Couleur couleur = cb_couleur.getSelectionModel().getSelectedItem();

        art.setArtSearched(nom_searched, volume, titrage_min, titrage_max, cnt, mar, fq, couleur, pays, type);
        tableArticle.setItems(art.filtrerArticle());
        tf_count_article.setText(String.valueOf(tableArticle.getItems().size()));
        ajust_slider();
    }

    private void ajust_slider() {
        sl_titrage_min.setMin(art.getTitrageMin());
        sl_titrage_min.setMax(art.getTitrageMax());

        sl_titrage_max.setMin(art.getTitrageMin());
        sl_titrage_max.setMax(art.getTitrageMax());

    }

    private void update_cb_marque(Fabriquant newValue) {

        System.out.println("Update fabricant");
        newValue.getListeMarque().add(0, new Marque(0, "Tous"));
        if(newValue.getId_fabriquant() == 0){
            cb_marque.setItems(FXCollections.observableArrayList(art.getListeMarqueFiltre()));

        }else {
            cb_marque.setItems(FXCollections.observableArrayList(newValue.getListeMarque()));
        }
        cb_marque.getSelectionModel().select(0);
        newValue.getListeMarque().remove(0);
    }

    private void update_cb_pays(Continent newValue) {
        newValue.getListePays().add(0, new Pays(0, "Tous"));
        if(newValue.getId_continent() == 0){
            cb_pays.setItems(FXCollections.observableArrayList(art.getListePaysFiltre()));

        }else {
            cb_pays.setItems(FXCollections.observableArrayList(newValue.getListePays()));
        }

        cb_pays.getSelectionModel().select(0);
        newValue.getListePays().remove(0);

    }

    public void setMain(SDBM sdbm) {
        this.sdbm = sdbm;
        art = this.sdbm.getArtBean();
        btn_modifier.setDisable(true);
        btn_supprimer.setDisable(true);
        cb_couleur.setItems(art.getListeCouleursFiltre());
        cb_couleur.getSelectionModel().select(0);
        cb_type.setItems(art.getListeTypeFiltre());
        cb_type.getSelectionModel().select(0);
        cb_pays.setItems(art.getListePaysFiltre());
        cb_pays.getSelectionModel().select(0);
        cb_continent.setItems(art.getListeContinentFiltre());
        cb_continent.getSelectionModel().select(0);
        cb_marque.setItems(art.getListeMarqueFiltre());
        cb_marque.getSelectionModel().select(0);
        cb_fabriquant.setItems(art.getListeFabricantFiltre());
        cb_fabriquant.getSelectionModel().select(0);
        ajust_slider();
        sl_titrage_max.setValue(art.getTitrageMax());
        sl_titrage_min.setValue(0.01);


        colonneNom.setCellValueFactory(
                (CellDataFeatures<Article, String> feature) -> feature.getValue().nom_articleProperty());
        colonnePrix.setCellValueFactory(
                (CellDataFeatures<Article, BigDecimal> feature) -> feature.getValue().prix_achatProperty());
        colonneVolume.setCellValueFactory(
                (CellDataFeatures<Article, Integer> feature) -> feature.getValue().volumeProperty().asObject());
        colonneMarque.setCellValueFactory(
                (CellDataFeatures<Article, Marque> feature) -> feature.getValue().marqueProperty());
        colonneFabriquant.setCellValueFactory(
                (CellDataFeatures<Article, Fabriquant> feature) -> feature.getValue().getMarque().fabriquantProperty());
        colonneType.setCellValueFactory((CellDataFeatures<Article, Type> feature) -> feature.getValue().typeProperty());
        colonneCouleur.setCellValueFactory(
                (CellDataFeatures<Article, Couleur> feature) -> feature.getValue().couleurProperty());
        colonneContinent.setCellValueFactory((CellDataFeatures<Article, Continent> feature) -> feature.getValue()
                .getMarque().getPays().continentProperty());
        colonnePays.setCellValueFactory(
                (CellDataFeatures<Article, Pays> feature) -> feature.getValue().getMarque().paysProperty());
        colonneTitrage.setCellValueFactory(
                (CellDataFeatures<Article, Float> feature) -> feature.getValue().titrageProperty().asObject());

        tableArticle.setItems(art.getSortedListArticle());
        art.getSortedListArticle().comparatorProperty().bind(tableArticle.comparatorProperty());

        // ajout ,loistener selected article
        tableArticle.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> setSelectedArticle(newValue));

        // ajout listener sur combobox continent
        cb_continent.valueProperty().addListener((observable, oldValue, newValue) -> update_cb_pays(newValue));

        // ajout listener sur combobox fabriquant
        cb_fabriquant.valueProperty().addListener((observable, oldValue, newValue) -> update_cb_marque(newValue));

        // ajout listener sur textfield recherche
        tf_recherche.textProperty().addListener((ObservableValue, old, t1) -> filtrerArticle());



        // ajout listener sur sliders si titrage MIN >MAX => ajustement
        sl_titrage_min.valueProperty().addListener((ObservableValue, oldValue, newValue) -> control_slider_min((Double) newValue));
        sl_titrage_max.valueProperty().addListener((ObservableValue, oldValue, newValue) -> control_slider_max((Double) newValue));
        tf_count_article.setText(String.valueOf(tableArticle.getItems().size()));



    }

    private void control_slider_min(Double newValue) {
        if (newValue > sl_titrage_max.getValue()) {

            sl_titrage_max.setValue(sl_titrage_min.getValue());

        }

    }

    private void control_slider_max(Double newValue) {
        if (newValue < sl_titrage_min.getValue()) {

            sl_titrage_min.setValue(sl_titrage_max.getValue());

        }

    }

    @FXML
    private void modif_article() {
        sdbm.showModiArticle();
        tableArticle.setItems(art.getSortedListArticle());

    }

    @FXML
    private void ajout_article() {
        sdbm.showAjoutArticle();
        tableArticle.setItems(art.getSortedListArticle());

    }

    @FXML
    public void delete_article() {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("SUPPRESSION");
        alert.setContentText("Etes vous sur de vouloir supprimer ?\n" + art.getSelectedArticle().toString());
        Optional<ButtonType> resultat = alert.showAndWait();
        if (resultat.isPresent() && resultat.get() == ButtonType.OK) {

            try {
                art.delete();
            } catch (Exception e) {
                e.printStackTrace();
                Alert erreur = new Alert(AlertType.ERROR);
                erreur.setContentText("Impossible de supprimer l'article !\nN'est il pas lié à une vente ?");
                erreur.showAndWait();
            }

        }

        tableArticle.setItems(art.getSortedListArticle());
    }


    public void imprimer() throws PrinterException {

        PrinterJob printerJob = PrinterJob.createPrinterJob();
        printerJob.showPrintDialog(sdbm.getStage());
        boolean success = printerJob.printPage(tableArticle);
        if (success) {
            printerJob.endJob();
        } else
            printerJob.cancelJob();

    }

}
