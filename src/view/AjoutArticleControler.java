package view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;

import com.jfoenix.controls.JFXSlider;
import com.sun.source.tree.NewClassTree;

import application.SDBM;
import dao.DAOFactory;
import javafx.beans.property.IntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import metier.Article;
import metier.Continent;
import metier.Couleur;
import metier.Fabriquant;
import metier.Marque;
import metier.Pays;
import metier.Type;
import service.ArticleBean;
import service.ArticleSearched;

public class AjoutArticleControler {

	private SDBM sdbm;
	private AnchorPane anchorPane;
	private ArticleBean art;
	Stage stageModif;

	@FXML
	private ComboBox<Couleur> cb_couleur;
	@FXML
	private ComboBox<Type> cb_type;
	@FXML
	private ComboBox<Marque> cb_marque;
	@FXML
	private TextField tf_nom;
	@FXML
	private TextField tf_prix_achat;
	@FXML
	private TextField tf_titrage;
	@FXML
	private RadioButton rb_33;
	@FXML
	private RadioButton rb_75;
	@FXML
	private ToggleGroup tg_volume;

	@FXML
	private void initialize() {
	}

	public void setMain(SDBM sdbm) {
		this.sdbm = sdbm;
		art = sdbm.getArtBean();
		cb_type.setItems(art.getListeType());
		cb_couleur.setItems(art.getListeCouleurs());
		if (!cb_type.getItems().toString().contains("Aucun")) {
			cb_type.getItems().add(0, new Type(0, "Aucun"));
		}

		cb_type.getSelectionModel().select(0);

		cb_couleur.setItems(art.getListeCouleurs());
		if (!cb_couleur.getItems().toString().contains("Aucune")) {
			cb_couleur.getItems().add(0, new Couleur(0, "Aucune"));

		}
		cb_couleur.getSelectionModel().select(0);
		cb_marque.setItems(art.getListeMarque());

	}

	public void setStage(Stage stageModif) {
		this.stageModif = stageModif;
	}

	@FXML
	public void save_ajout() {
		Boolean isValueEntered = true;
		isValueEntered = getValueEntered(isValueEntered);

		if (isValueEntered) {
			Article newArticle = getNewArticle();

			System.out.println("NEW ARTICLE  :" + newArticle);

			art.saveAjout(newArticle);
			stageModif.close();
		}else{
			showErrorAlert();
		}
	}

	private void showErrorAlert() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Champ incorrect");
		alert.setContentText("Les champs NOM PRIX ET MARQUE doivent être remplit ");
		alert.showAndWait();
	}

	private Article getNewArticle() {
		Article newArticle = new Article(0, null, null, 0, (float) 0);

		newArticle.setNom_article(tf_nom.getText());
		newArticle.setPrix_achat(new BigDecimal(tf_prix_achat.getText()));
		if (tf_titrage.getText().isEmpty()) {
			newArticle.setTitrage(0);

		} else {
			newArticle.setTitrage(Float.parseFloat(tf_titrage.getText()));
		}

		newArticle.setCouleur(cb_couleur.getSelectionModel().getSelectedItem());
		newArticle.setMarque(cb_marque.getSelectionModel().getSelectedItem());

		if (cb_type.getSelectionModel().equals(null)) {
			newArticle.setType(null);

		} else {
			newArticle.setType(cb_type.getSelectionModel().getSelectedItem());
		}

		if (tg_volume.getSelectedToggle() == rb_33) {
			newArticle.setVolume(33);

		} else if (tg_volume.getSelectedToggle() == rb_75) {
			newArticle.setVolume(75);
		} else {
			newArticle.setVolume(0);
		}
		return newArticle;
	}

	private Boolean getValueEntered(Boolean isValueEntered) {
		if (tf_nom.getText().isEmpty() || tf_prix_achat.getText().isEmpty() || cb_marque.getSelectionModel().getSelectedItem().equals(null)) {
			isValueEntered = false;
		}
		return isValueEntered;
	}


	@FXML
	public void cancel() {
		stageModif.close();

		
	}
	
	
}
