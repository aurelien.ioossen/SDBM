package view;

import application.SDBM;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import metier.Article;
import metier.Couleur;
import metier.Marque;
import metier.Type;
import service.ArticleBean;

import java.math.BigDecimal;

public class ModifArticleControler {

    Stage stageModif;
    private SDBM sdbm;
    private AnchorPane anchorPane;
    private ArticleBean art;
    @FXML
    private ComboBox<Couleur> cb_couleur;
    @FXML
    private ComboBox<Type> cb_type;
    @FXML
    private ComboBox<Marque> cb_marque;
    @FXML
    private TextField tf_nom;
    @FXML
    private TextField tf_prix_achat;
    @FXML
    private TextField tf_titrage;
    @FXML
    private RadioButton rb_33;
    @FXML
    private RadioButton rb_75;
    private Article selectedArticle;
    @FXML
    private ToggleGroup tg_volume;

    @FXML
    private void initialize() {
    }

    public void setMain(SDBM sdbm) {
        this.sdbm = sdbm;

        art = SDBM.getInstance().getArtBean();
        selectedArticle = art.getSelectedArticle();

        cb_type.setItems(art.getListeType());


        if (!cb_type.getItems().toString().contains("Aucun")) {
            cb_type.getItems().add(0, new Type(0, "Aucun"));
        }
        cb_type.setValue(selectedArticle.getType());


        cb_couleur.setItems(art.getListeCouleurs());
        if (!cb_couleur.getItems().toString().contains("Aucune")) {
            cb_couleur.getItems().add(0, new Couleur(0, "Aucune"));

        }

        cb_couleur.setValue(art.getSelectedArticle().getCouleur());
        cb_marque.setItems(art.getListeMarque());
        cb_marque.setValue(selectedArticle.getMarque());
        tf_nom.setText(selectedArticle.getNom_article());
        tf_prix_achat.setText(selectedArticle.getPrix_achat().toString());
        tf_titrage.setText(Float.toString(selectedArticle.getTitrage()));

        if (selectedArticle.getVolume() == 33) {
            rb_33.setSelected(true);

        } else {
            rb_75.setSelected(true);

        }
    }

    public void setStage(Stage stageModif) {
        this.stageModif = stageModif;
    }


    @FXML
    public void save_modif() {
        Boolean isValueEntered = true;
        if (tf_nom.getText().isEmpty() || tf_prix_achat.getText().isEmpty() || cb_marque.getSelectionModel().getSelectedItem().equals(null)) {
            isValueEntered = false;
        }

        if (isValueEntered) {
            selectedArticle.setNom_article(tf_nom.getText());
            selectedArticle.setPrix_achat(new BigDecimal(tf_prix_achat.getText()));
            selectedArticle.setTitrage(Float.parseFloat(tf_titrage.getText()));
            selectedArticle.setCouleur(cb_couleur.getSelectionModel().getSelectedItem());
            selectedArticle.setMarque(cb_marque.getSelectionModel().getSelectedItem());
            selectedArticle.setType(cb_type.getSelectionModel().getSelectedItem());
            if (tg_volume.getSelectedToggle() == rb_33) {
                selectedArticle.setVolume(33);

            } else if (tg_volume.getSelectedToggle() == rb_75) {
                selectedArticle.setVolume(75);
            } else {
                selectedArticle.setVolume((Integer) null);
            }

            System.out.println("NEW ARTICLE SELECTED :" + selectedArticle);

            art.saveModif();
            stageModif.close();
        }else{
        	Alert alert = new Alert(Alert.AlertType.ERROR);
        	alert.setTitle("Champ incorrect");
        	alert.setContentText("Les champs NOM PRIX ET MARQUE doivent être remplit ");
        	alert.showAndWait();
		}
    }


    @FXML
    public void cancel() {
        stageModif.close();


    }

}


