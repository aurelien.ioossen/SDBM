package metier;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;

public class Fabriquant {

	
	private int id_fabriquant;
	private String fabriquant ;
	private ArrayList<Marque> listeMarque;
	
	
	public Fabriquant(int id_fabriquant, String fabriquant) {
		this.id_fabriquant = id_fabriquant;
		this.fabriquant=fabriquant;
		listeMarque = new ArrayList<>() ;
	}



	public void AddMarque(Marque m) {
		
		listeMarque.add(m);

		
	}

	public void RemoveMarque(Marque m) {
		
		listeMarque.remove(m);

		
	}
	

	
	
	public ArrayList<Marque> getListeMarque() {
		return listeMarque;
	}



	public int getId_fabriquant() {
		return id_fabriquant;
	}





	public String getFabriquant() {
		return fabriquant;
	}





	@Override
	public String toString() {
		return fabriquant;
	}



	public SimpleStringProperty getFabriquantProperty() {
		return new SimpleStringProperty(fabriquant);
	};
	
	
	
	
	
}
