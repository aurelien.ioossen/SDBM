package metier;

import javafx.beans.property.SimpleStringProperty;

public class Couleur {

	
	private int id_couleur;
	private String couleur ;
	
	
	public Couleur(int id_couleur, String couleur) {
		this.id_couleur = id_couleur;
		this.couleur = couleur;
	}


	public int getId_couleur() {
		return id_couleur;
	}


	public String getCouleur() {
		return couleur;
	}


	@Override
	public String toString() {
		return  couleur;
	}


	public SimpleStringProperty getCouleurProperty() {
		return new SimpleStringProperty(couleur);
	};
	
	
	
	
	
}
