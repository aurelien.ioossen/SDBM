package metier;

import java.math.BigDecimal;

import javafx.beans.InvalidationListener;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class Article {

	private IntegerProperty id_article;
	private StringProperty nom_article;
	private ObjectProperty<BigDecimal> prix_achat ;
	private IntegerProperty volume;
	private FloatProperty titrage;
	private ObjectProperty<Marque> marque;
	private ObjectProperty<Couleur> couleur;
	private ObjectProperty<Type> type;
	
	public Article(int id_article, String nom_article, BigDecimal prix_achat,
			int volume, Float titrage) {
		super();

	this.id_article = new SimpleIntegerProperty(id_article);
	this.nom_article= new SimpleStringProperty(nom_article);
	this.prix_achat = new SimpleObjectProperty<>(prix_achat);	
	this.volume = new SimpleIntegerProperty(volume);
	this.titrage = new SimpleFloatProperty(titrage);

	this.marque = new SimpleObjectProperty<>() ;
	this.couleur = new SimpleObjectProperty<>();
	this.type = new SimpleObjectProperty<>() ;

	
		

	}

	


	@Override
	public String toString() {
		return "Article [id_article=" + id_article + ", nom_article=" + nom_article + ", prix_achat=" + prix_achat
				+ ", volume=" + volume + ", titrage=" + titrage + ", marque=" + marque + ", couleur=" + couleur
				+ ", type=" + type + "]";
	}


	public final IntegerProperty id_articleProperty() {
		return this.id_article;
	}
	


	public final int getId_article() {
		return this.id_articleProperty().get();
	}
	


	public final void setId_article(final int id_article) {
		this.id_articleProperty().set(id_article);
	}
	


	public final StringProperty nom_articleProperty() {
		return this.nom_article;
	}
	


	public final String getNom_article() {
		return this.nom_articleProperty().get();
	}
	


	public final void setNom_article(final String nom_article) {
		this.nom_articleProperty().set(nom_article);
	}
	


	public final ObjectProperty<BigDecimal> prix_achatProperty() {
		return this.prix_achat;
	}
	


	public final BigDecimal getPrix_achat() {
		return this.prix_achatProperty().get();
	}
	


	public final void setPrix_achat(final BigDecimal prix_achat) {
		this.prix_achatProperty().set(prix_achat);
	}
	


	public final IntegerProperty volumeProperty() {
		return this.volume;
	}
	


	public final int getVolume() {
		return this.volumeProperty().get();
	}
	


	public final void setVolume(final int volume) {
		this.volumeProperty().set(volume);
	}
	


	public final FloatProperty titrageProperty() {
		return this.titrage;
	}
	


	public final float getTitrage() {
		return this.titrageProperty().get();
	}
	


	public final void setTitrage(final float titrage) {
		this.titrageProperty().set(titrage);
	}
	


	public final ObjectProperty<Marque> marqueProperty() {
		return this.marque;
	}
	


	public final Marque getMarque() {
		return this.marqueProperty().get();
	}
	


	public final void setMarque(final Marque marque) {
		this.marqueProperty().set(marque);
	}
	


	public final ObjectProperty<Couleur> couleurProperty() {
		return this.couleur;
	}
	


	public final Couleur getCouleur() {
		return this.couleurProperty().get();
	}
	


	public final void setCouleur(final Couleur couleur) {
		this.couleurProperty().set(couleur);
	}
	


	public final ObjectProperty<Type> typeProperty() {
		return this.type;
	}
	


	public final Type getType() {
		return this.typeProperty().get();
	}
	


	public final void setType(final Type type) {
		this.typeProperty().set(type);
	}
	


	

	
	
}
