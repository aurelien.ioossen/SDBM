package metier;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Pays {

	
	private int id_pays;
	private ObjectProperty<Continent> continent;
	private String pays ;
	
	
	public Pays(int id_pays, String pays,Continent continent) {
		this.id_pays = id_pays;
		this.continent = new SimpleObjectProperty<>(continent);
		this.pays=pays;
	}


	public Pays(int id_pays, String pays) {
		this.id_pays = id_pays;
		this.pays = pays;
		
	}




	public int getId_pays() {
		return id_pays;
	}
	
	

	public String getPays() {
		return pays;
	}


	@Override
	public String toString() {
		return pays;
	}


	public SimpleStringProperty getPaysProperty() {
		return new SimpleStringProperty(pays);
	}


	public final ObjectProperty<Continent> continentProperty() {
		return this.continent;
	}
	


	public final Continent getContinent() {
		return this.continentProperty().get();
	}
	


	public final void setContinent(final Continent continent) {
		this.continentProperty().set(continent);
	}
	
	
	
	
	
	
	
	
}
