package metier;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;

public class Continent {

	
	private int id_continent;
	private String continent ;
	private ArrayList<Pays> listePays ;
	
	

	public ArrayList<Pays> getListePays() {
		return listePays;
	}


	public Continent(int id_continent, String continent) {
		this.id_continent = id_continent;
		this.continent=continent;
		listePays = new ArrayList<>() ;

	}


	public void AddPays(Pays p) {
		
		listePays.add(p);

		
	}

	public void RemovePays(Pays p) {
		
		listePays.remove(p);

		
	}

	public int getId_continent() {
		return id_continent;
	}


	public String getContinent() {
		return continent;
	}


	@Override
	public String toString() {
		return continent;
	}


	public SimpleStringProperty getContinentProperty() {
		return new SimpleStringProperty(continent);
	};
	
	
	
	
	
}
