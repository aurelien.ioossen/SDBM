package metier;

import javafx.beans.property.SimpleStringProperty;

public class Type {

	
	private int id_type;
	private String type ;
	
	
	public Type(int id_type, String type) {
		this.id_type = id_type;
		this.type = type;
	}


	public int getId_type() {
		return id_type;
	}


	public String getType() {
		return type;
	}


	@Override
	public String toString() {
		return  type;
	}


	public SimpleStringProperty getTypeProperty() {
		return new SimpleStringProperty(type);
	};
	
	
	
	
	
}
