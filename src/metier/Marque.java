package metier;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Marque {

	
	private int id_marque;
	private ObjectProperty<Pays> pays;
	private ObjectProperty<Fabriquant> fabriquant;
	private String marque ;
	
	
	public Marque(int id_marque, String marque,Pays pays,Fabriquant fabriquant) {
		this.pays = new SimpleObjectProperty<>(pays);
		this.fabriquant = new SimpleObjectProperty<>(fabriquant);
		this.id_marque = id_marque;
		this.marque=marque;
	}







	public Marque(int id_marque, String marque) {
		this.id_marque = id_marque;
		this.marque=marque;
	}







	public int getId_marque() {
		return id_marque;
	}




	public String getNomMarque() {
		return marque;
	}

	public SimpleStringProperty getNomMarqueProperty() {
		return new SimpleStringProperty(marque);
	}





	@Override
	public String toString() {
		return marque;
	}







	public final ObjectProperty<Fabriquant> fabriquantProperty() {
		return this.fabriquant;
	}
	







	public final Fabriquant getFabriquant() {
		return this.fabriquantProperty().get();
	}
	







	public final void setFabriquant(final Fabriquant fabriquant) {
		this.fabriquantProperty().set(fabriquant);
	}







	public final ObjectProperty<Pays> paysProperty() {
		return this.pays;
	}
	







	public final Pays getPays() {
		return this.paysProperty().get();
	}
	







	public final void setPays(final Pays pays) {
		this.paysProperty().set(pays);
	}
	
	
	
	
	
	
	
}
