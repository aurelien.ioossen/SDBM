package service;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import metier.Article;
import metier.Continent;
import metier.Couleur;
import metier.Fabriquant;
import metier.Marque;
import metier.Pays;
import metier.Type;

public class ArticleBean {

	private ObservableList<Couleur> listeCouleurs;
	private ObservableList<Couleur> listeCouleursFiltre;	
	private ObservableList<Type> listeType;
	private ObservableList<Type> listeTypeFiltre;
	private ObservableList<Marque> listeMarque;
	private ObservableList<Marque> listeMarqueFiltre;
	private ObservableList<Fabriquant> listeFabricant;
	private ObservableList<Fabriquant> listeFabricantFiltre;
	private ObservableList<Pays> listePays;
	private ObservableList<Pays> listePaysFiltre;
	private ObservableList<Continent> listeContinent;
	private ObservableList<Continent> listeContinentFiltre;
	private ObservableList<Article> listeArticle;
	private SortedList<Article> sortedListArticle;
	private ArticleSearched artSearched;
	private Article selectedArticle;



	public ArticleBean() {

		listeCouleurs = FXCollections.observableArrayList(DAOFactory.getInstanceCouleurDAO().getAll());
		listePays = FXCollections.observableArrayList(DAOFactory.getInstancePaysDAO().getAll());
		listeContinent = FXCollections.observableArrayList(DAOFactory.getInstanceContinentDAO().getAll());
		listeType = FXCollections.observableArrayList(DAOFactory.getInstanceTypeDAO().getAll());
		listeFabricant = FXCollections.observableArrayList(DAOFactory.getInstanceFabriquantDAO().getAll());
		listeMarque = FXCollections.observableArrayList(DAOFactory.getInstanceMarqueDAO().getAll());
	
		listeCouleursFiltre = FXCollections.observableArrayList(new ArrayList<>());
		listePaysFiltre = FXCollections.observableArrayList(new ArrayList<>());
		listeContinentFiltre = FXCollections.observableArrayList(new ArrayList<>());
		listeTypeFiltre = FXCollections.observableArrayList(new ArrayList<>());
		listeFabricantFiltre = FXCollections.observableArrayList(new ArrayList<>());
		listeMarqueFiltre = FXCollections.observableArrayList(new ArrayList<>());


		listeCouleursFiltre.addAll(listeCouleurs) ;
		listeCouleursFiltre.add(0, new Couleur(0, "Toutes"));
		
		listePaysFiltre.addAll(listePays);
		listePaysFiltre.add(0, new Pays(0, "Tous"));
		
		listeContinentFiltre.addAll(listeContinent);
		listeContinentFiltre.add(0, new Continent(0, "Tous"));
		
		listeTypeFiltre.addAll(listeType) ;
		listeTypeFiltre.add(0, new Type(0, "Tous"));
		
		listeFabricantFiltre.addAll(listeFabricant);
		listeFabricantFiltre.add(0, new Fabriquant(0, "Tous"));
		
		listeMarqueFiltre.addAll(listeMarque);
		listeMarqueFiltre.add(0, new Marque(0, "Tous"));

		listeArticle = FXCollections.observableArrayList(DAOFactory.getInstanceArticleDAO().getAll());
		sortedListArticle = new SortedList<>(listeArticle);

		artSearched = new ArticleSearched(null, 0, 0, 26, null, null, null, null, null);
		selectedArticle = new Article(0, "", null, 0, (float) 0);

	}

	
	
	
	
	public ObservableList<Couleur> getListeCouleursFiltre() {
		return listeCouleursFiltre;
	}





	public ObservableList<Type> getListeTypeFiltre() {
		return listeTypeFiltre;
	}





	public ObservableList<Marque> getListeMarqueFiltre() {
		return listeMarqueFiltre;
	}





	public ObservableList<Fabriquant> getListeFabricantFiltre() {
		return listeFabricantFiltre;
	}





	public ObservableList<Pays> getListePaysFiltre() {
		return listePaysFiltre;
	}





	public ObservableList<Continent> getListeContinentFiltre() {
		return listeContinentFiltre;
	}





	public ObservableList<Type> getListeType() {
		return listeType;
	}

	public ObservableList<Marque> getListeMarque() {
		return listeMarque;
	}

	public ObservableList<Fabriquant> getListeFabricant() {
		return listeFabricant;
	}

	public ObservableList<Pays> getListePays() {
		return listePays;
	}

	public ObservableList<Continent> getListeContinent() {
		return listeContinent;
	}

	public ObservableList<Couleur> getListeCouleurs() {
		return listeCouleurs;
	}

	public SortedList<Article> getSortedListArticle() {
		return sortedListArticle;
	}

	public ArticleSearched getArtSearched() {
		return artSearched;
	}

	public void setArtSearched(String nom_searched, Integer volume, double titrage_min, double titrage_max,
			Continent continent, Marque marque, Fabriquant fabriquant, Couleur couleur, Pays pays, Type type) {
		artSearched.setNomSearched(nom_searched);
		artSearched.setVolume(volume);
		artSearched.setTitrageMin(titrage_min);
		artSearched.setTitrageMax(titrage_max);
		artSearched.setContinent(continent);
		artSearched.setFabriquant(fabriquant);
		artSearched.setCouleur(couleur);
		artSearched.setPays(pays);
		artSearched.setMarque(marque);
		artSearched.setType(type);
	}

	public ObservableList<Article> filtrerArticle() {

		return listeArticle = DAOFactory.getInstanceArticleDAO().filtrerArticle(artSearched);

	}

	public double getTitrageMin() {
		return DAOFactory.getInstanceArticleDAO().getTitrageMin(artSearched);
	}

	public double getTitrageMax() {
		return DAOFactory.getInstanceArticleDAO().getTitrageMax(artSearched);
	}

	
	public Article getSelectedArticle() {
		return selectedArticle;
	}

	public void setSelectedArticle(Article selectedArticle) {
		this.selectedArticle = selectedArticle;
	}

	@Override
	public String toString() {
		return "ArticleBean [listeCouleurs=" + listeCouleurs + ", listeType=" + listeType + ", listeMarque="
				+ listeMarque + ", listeFabricant=" + listeFabricant + ", listePays=" + listePays + ", listeContinent="
				+ listeContinent + ", listeArticle=" + listeArticle + ", sortedListArticle=" + sortedListArticle
				+ ", artSearched=" + artSearched + ", selectedArticle=" + selectedArticle + "]";
	}

	public void saveModif() {

		DAOFactory.getInstanceArticleDAO().update(selectedArticle);
	}





	public void delete() throws SQLException {
		DAOFactory.getInstanceArticleDAO().delete(selectedArticle);		
	}





	public void saveAjout(Article newArticle) {
		DAOFactory.getInstanceArticleDAO().insert(newArticle);		
		
	}

}
