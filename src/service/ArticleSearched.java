package service;

import metier.Continent;
import metier.Couleur;
import metier.Fabriquant;
import metier.Marque;
import metier.Pays;
import metier.Type;

public class ArticleSearched {

	private String nomSearched;
	private Integer volume;
	private Double titrageMin;
	private Double titrageMax;
	private Continent continent;
	private Marque marque;
	private Fabriquant fabriquant;
	private Couleur couleur;
	private Pays pays;
	private Type type;

	public ArticleSearched(String nomSearched, Integer volume, double titrage_min, double titrage_max,
			Continent continent, Fabriquant fabriquant, Couleur couleur, Pays pays, Type type) {
		super();
		this.nomSearched = nomSearched;
		this.volume = volume;
		this.titrageMin = titrage_min;
		this.titrageMax = titrage_max;
		this.continent = continent;
		this.fabriquant = fabriquant;
		this.couleur = couleur;
		this.pays = pays;
		this.type=type;
	}

	public String getNomSearched() {
		return nomSearched;
	}

	public void setNomSearched(String nomSearched) {
		this.nomSearched = nomSearched;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public Double getTitrageMin() {
		return titrageMin;
	}

	public void setTitrageMin(Double titrageMin) {
		this.titrageMin = titrageMin;
	}

	public Double getTitrageMax() {
		return titrageMax;
	}

	public void setTitrageMax(Double titrageMax) {
		this.titrageMax = titrageMax;
	}

	public Continent getContinent() {
		return continent;
	}

	public void setContinent(Continent continent) {
		this.continent = continent;
	}

	public Fabriquant getFabriquant() {
		return fabriquant;
	}

	public void setFabriquant(Fabriquant fabriquant) {
		this.fabriquant = fabriquant;
	}

	public Couleur getCouleur() {
		return couleur;
	}

	public void setCouleur(Couleur couleur) {
		this.couleur = couleur;
	}

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}

	public void setMarque(Marque marque) {
		this.marque = marque;
	}

	public Marque getMarque() {
		return marque;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

}
