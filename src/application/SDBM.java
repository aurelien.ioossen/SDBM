package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import service.ArticleBean;
import view.AjoutArticleControler;
import view.ModifArticleControler;
import view.SdbmControler;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class SDBM extends Application {



	private static SDBM mainapp ;
	private Stage primaryStage;
	private AnchorPane root;
	private ArticleBean art;



	@Override
	public void start(Stage primaryStage) {
		mainapp =this;
		art = new ArticleBean();
		this.primaryStage = primaryStage;
		primaryStage.setTitle("GESTION ARTICLE SDBM");
		primaryStage.show();
		showAppli();

	}

	private void showAppli() {
		try {
			FXMLLoader myFXMLloader = new FXMLLoader();
			myFXMLloader.setLocation(SDBM.class.getResource("/SDBM_FXML.fxml"));
			root = myFXMLloader.load();
			// D?claration et instanciation de la sc?ne contenant le layout
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			// primaryStage.setFullScreen(true);
			primaryStage.setScene(scene);
			// D?marrage du Stage
			// D?claration et instanciation du controller
			SdbmControler controler = myFXMLloader.getController();
			controler.setMain(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);
	}

	public void showModiArticle() {
		AnchorPane rootModif = new AnchorPane();
		Stage stageModif = new Stage();
		stageModif.setTitle("Modification Article");
		FXMLLoader myFXMLloader2 = new FXMLLoader();
		myFXMLloader2.setLocation(SDBM.class.getResource("/vue_modif_article.fxml"));
		try {
			rootModif = myFXMLloader2.load();
			stageModif.initModality(Modality.WINDOW_MODAL);
			stageModif.initOwner(primaryStage);
			Scene sceneModif = new Scene(rootModif);
			stageModif.setScene(sceneModif);
			ModifArticleControler articleModifController = myFXMLloader2.getController();
			articleModifController.setMain(this);
			articleModifController.setStage(stageModif);
			stageModif.showAndWait();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public ArticleBean getArtBean() {
		return art;
	}

	public void setArtBean(ArticleBean art) {
		this.art = art;
	}

	public void showAjoutArticle() {
		AnchorPane rootModif = new AnchorPane();
		Stage stageModif = new Stage();
		stageModif.setTitle("Ajout Article");
		FXMLLoader myFXMLloader2 = new FXMLLoader();
		myFXMLloader2.setLocation(SDBM.class.getResource("/vue_ajout_article.fxml"));
		try {
			rootModif = myFXMLloader2.load();
			stageModif.initModality(Modality.WINDOW_MODAL);
			stageModif.initOwner(primaryStage);
			Scene sceneModif = new Scene(rootModif);
			stageModif.setScene(sceneModif);
			AjoutArticleControler articleModifController = myFXMLloader2.getController();
			articleModifController.setMain(this);
			articleModifController.setStage(stageModif);
			stageModif.showAndWait();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public Stage getStage() {
		return primaryStage;
	}

	public static SDBM getInstance() {
		return mainapp;
	}

	
	
}
